import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by ����� on 22.03.2016.
 */
public class FileRenderer extends DefaultTableCellRenderer {
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        ImageIcon icon = new ImageIcon(getClass().getResource("npb.png"));
        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("Folder")) {
            icon = new ImageIcon(getClass().getResource("Folder.png"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("bin") || getFileExtension(table.getValueAt(row, 1).toString()).equals("BIN")) {
            icon = new ImageIcon(getClass().getResource("Bin.jpg"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("doc") || getFileExtension(table.getValueAt(row, 1).toString()).equals("docx")) {
            icon = new ImageIcon(getClass().getResource("doc.png"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("djvu") || getFileExtension(table.getValueAt(row, 1).toString()).equals("DJVU")) {
            icon = new ImageIcon(getClass().getResource("djvu.png"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("jpg") || getFileExtension(table.getValueAt(row, 1).toString()).equals("JPG")) {
            icon = new ImageIcon(getClass().getResource("jpg.jpg"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("avi")) {
            icon = new ImageIcon(getClass().getResource("avi.jpg"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("mp4")) {
            icon = new ImageIcon(getClass().getResource("mp4.jpg"));
        }

        if(getFileExtension(table.getValueAt(row, 1).toString()).equals("png") || getFileExtension(table.getValueAt(row, 1).toString()).equals("PNG")) {
            icon = new ImageIcon(getClass().getResource("png.jpg"));
        }
        setIcon(new ImageIcon(icon.getImage().getScaledInstance(20, 30, icon.getImage().SCALE_DEFAULT)));
        return this;
    }

    private static String getFileExtension(String file) {
        if (file.lastIndexOf(".") != -1 && file.lastIndexOf(".") != 0 && !file.substring((file.lastIndexOf(".") + 1), file.lastIndexOf(".") + 2).equals(" "))
            return file.substring(file.lastIndexOf(".") + 1);
        else return "Folder";
    }

}
