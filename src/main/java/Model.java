import java.io.File;

public interface Model {

    boolean returnBool();

    void goToChild(int childNumber);

    void setModelEvent(ModelEvent modelEvent);

    int getCount();

    File getFile(int rowIndex);

    boolean backpath();
}
