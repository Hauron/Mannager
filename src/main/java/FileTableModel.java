import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.io.File;

public class FileTableModel extends AbstractTableModel {
    private JLabel folder = new JLabel(new ImageIcon("Folder.png"));
    private boolean temp = true;
    Model model;

    public FileTableModel(Model model) {
        this.model = model;
    }

    public int getRowCount() {
        return model.getCount();
    }

    public void changeTemp() {
        temp = true;
    }

    public int getColumnCount() {
        return 3;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class result;
        switch (columnIndex) {
            case 0:
                result = File.class;
                break;
            default:
                result = String.class;
        }
        return result;
    }

    public String getColumnName(int column) {
        String result = "";
        if (column == 0) result = "File";
        if (column == 1) result = "Name file";
        if (column == 2) result = "Size";
        return result;
    }


    public Object getValueAt(int rowIndex, int columnIndex) {
        Object result = "";
        File currentRowFile = model.getFile(rowIndex);
        if (columnIndex == 0) {
            return currentRowFile;
        }
        if (columnIndex == 1) {
            result = currentRowFile.getPath();
        }
        if(columnIndex == 2) {
            if(temp) {
                result = currentRowFile.getTotalSpace();
            }
            else
                result = space(currentRowFile);
        }
        return result;
    }
    

    public long space(File currentRowFile) {
        temp = false;
        long temp = 0;
        Queue queue = new Queue();
        queue.add(currentRowFile);
        while (!queue.queueEmpty()) {
            File t = queue.pull();
            File[] arr = t.listFiles();
            if(arr != null) {
                for(int i = 0; i < arr.length; i++) {
                    File j = arr[i];
                    queue.add(j);
                }
            }
            else temp += t.length();
        }
        return temp;

    }


}
