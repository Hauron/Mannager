import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
//377
public class Main {
    private static File tem;
    private static boolean gj = true;
    private static JTable jTable;
    private static JButton size = new JButton("size");

    public static void main(String[] args) {

        JFrame jFrame = new JFrame();
        JPanel jPanel = new JPanel(new BorderLayout());
        JPanel forButtons = new JPanel(new FlowLayout());
        JButton back = new JButton("back");
        size.setVisible(false);
        forButtons.add(back);
        forButtons.add(size);

        final Model localFileModel = new LocalFileModel();

        final FileTableModel tableModel = new FileTableModel(localFileModel);
        jTable = new JTable(tableModel);

        localFileModel.setModelEvent(new ModelEvent() {
            public void execute() {
                tableModel.fireTableDataChanged();
            }
        });

        jTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    if (e.getClickCount() == 2) {
                        localFileModel.goToChild(jTable.getSelectedRow());
                        if(gj) {
                            tem = null;
                            for (int i = 0; i < localFileModel.getCount(); i++) {
                                tem = localFileModel.getFile(i);
                                addListenerLength(size, tableModel);
                                size.setVisible(true);
                                gj = false;
                                System.out.println(tem);
                            }
                        }
                    }
                }
                catch (ArrayIndexOutOfBoundsException el) {

                }
            }
        });

        jTable.setDefaultRenderer(
                // �������� ����� ��� �������� ����������� �� ��� ����������� �������
                //������� � ������ ��� ���� ������
                File.class,
                new FileRenderer()

        );


        addListener(back, localFileModel, tableModel);
        jFrame.add(jPanel);
        JScrollPane scrollPane = new JScrollPane(jTable);
        jPanel.add(scrollPane);
        jPanel.add(forButtons, BorderLayout.NORTH);
        jFrame.setLocationRelativeTo(null);
        jFrame.setSize(500, 500);
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    public static void addListener(final JButton button, final Model localFileModel, final FileTableModel fileTableModel) {
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!localFileModel.returnBool())
                    fileTableModel.changeTemp();
                size.setVisible(localFileModel.backpath());
                gj = true;
            }
        });
    }

    public static void addListenerLength(final JButton button, final FileTableModel fileTableModel) {
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileTableModel.space(tem);
                fileTableModel.fireTableDataChanged();
            }
        });
    }

}
