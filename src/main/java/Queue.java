import java.io.File;
import java.util.Arrays;

/**
 * Created by ����� on 27.03.2016.
 */
public class Queue {
    private File[] queue;
    private int maxSize;
    private int front;
    private int rear;

    public Queue() {
        maxSize = 1;
        queue = new File[maxSize];
        front = 0;
        rear = -1;
    }

    public void add(File file) {
        int k = rear + 1;
        if(maxSize == k) {
            maxSize = queue.length + 10;
            queue = Arrays.copyOf(queue, maxSize);
        }
        queue[++rear] = file;
    }

    public File pull() {
        if(front > rear)
            return null;
        else
            return queue[front++];
    }

    public boolean queueEmpty() {
        return front > rear;
    }

}
