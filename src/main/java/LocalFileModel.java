import javax.swing.*;
import java.io.File;
import java.util.Arrays;

public class LocalFileModel implements Model {
    private File[] directoryFiles;
    private File lastdirectoryFiles;
    public boolean temp;

    private ModelEvent modelEvent;

    public LocalFileModel() {
        directoryFiles = File.listRoots();
        temp = false;
    }

    public void goToChild(int childNumber) {
        if(getMyFileExtension(getFile(childNumber).getName()).equals("Folder") && getFile(childNumber).listFiles() != null){
            lastdirectoryFiles = getFile(childNumber);
            directoryFiles = lastdirectoryFiles.listFiles();
            temp = true;
        }
        sendChangedEvent();
    }

    public int getCount() {
        return directoryFiles.length;
    }


    public boolean returnBool() {
        try {
            return !Arrays.equals(directoryFiles, File.listRoots()) && getMyPath(lastdirectoryFiles.getPath()) != null;
        }
        catch (StringIndexOutOfBoundsException e) {
            return false;
        }
    }


    public boolean backpath () {
        boolean bool = true;
        try {
            if (!Arrays.equals(directoryFiles, File.listRoots()) && getMyPath(lastdirectoryFiles.getPath()) != null) {
                System.out.println(getMyPath(lastdirectoryFiles.getPath()));
                directoryFiles = new File(str(lastdirectoryFiles)).listFiles();
                lastdirectoryFiles = new File(getMyPath(lastdirectoryFiles.getPath()));
            } else {
                directoryFiles = File.listRoots();
                bool = false;
            }
        }
        catch (StringIndexOutOfBoundsException e) {
            directoryFiles = File.listRoots();
            bool = false;
        }
        sendChangedEvent();
        return bool;
    }

    public File getFile(int rowIndex) {
        return directoryFiles[rowIndex];
    }


    private String str(File file) {
        return getMyPath(file.getPath());
    }

    private void sendChangedEvent() {
        if (modelEvent != null)
            modelEvent.execute();
    }

    public void setModelEvent(ModelEvent modelEvent) {
        this.modelEvent = modelEvent;
    }

    private static String getMyFileExtension(String file) {
        if (file.lastIndexOf(".") != -1 && file.lastIndexOf(".") != 0 && !file.substring((file.lastIndexOf(".") + 1), file.lastIndexOf(".") + 2).equals(" "))
            return file.substring(file.lastIndexOf(".") + 1);
        else return "Folder";
    }

    private static String getMyPath(String file) {
        if (file.lastIndexOf("\\") != -1 && file.lastIndexOf("\\") != 0 && !file.substring((file.lastIndexOf("\\") + 1), file.lastIndexOf("\\") + 2).equals(" "))
            return file.substring(0, file.lastIndexOf("\\"));
        return null;
    }


}
