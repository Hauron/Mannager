public interface ModelEvent {

    void execute();

}
